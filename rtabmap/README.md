#build from source

This section shows how to install RTAB-Map ros-pkg on ROS Hydro/Indigo/Jade (Catkin build). RTAB-Map works only with the PCL 1.7, which is the default version installed with ROS Hydro/Indigo/Jade (Fuerte and Groovy are not supported).

The next instructions assume that you have set up your ROS workspace using this tutorial. I will use indigo prefix for convenience, but it should work with hydro and jade. The workspace path is ~/catkin_ws and your ~/.bashrc contains:

$ source /opt/ros/indigo/setup.bash
$ source ~/catkin_ws/devel/setup.bash
Make sure you don't have the binaries installed too (if you tried them before):

$ sudo apt-get remove ros-indigo-rtabmap
Optional dependencies

If you want SURF/SIFT on Indigo/Jade (Hydro has already SIFT/SURF), you have to build OpenCV from source to have access to nonfree module. Install it in /usr/local (default) and the rtabmap library should link with it instead of the one installed in ROS. I recommend to use latest 2.4 version (2.4.11) and build it from source following these instructions. RTAB-Map can build with OpenCV3+xfeatures2d module, but rtabmap_ros package will have libraries conflict as cv-bridge is depending on OpenCV2. If you want OpenCV3, you should build ros vision-opencv package yourself (and all ros packages depending on it) so it can link on OpenCV3.

ROS (Qt, PCL, dc1394, OpenNI, OpenNI2, Freenect, g2o, Costmap2d, Rviz, Octomap, CvBridge). Note that I've found that latest g2o version built from source is faster (install libsuitesparse-dev before building g2o) and would be required to avoid some crashes.

$ sudo apt-get install libqt4-dev libpcl-1.7-all-dev libdc1394-dev ros-indigo-openni-launch ros-indigo-openni2-launch ros-indigo-freenect-launch ros-indigo-costmap-2d ros-indigo-octomap-ros ros-indigo-g2o ros-indigo-rviz ros-indigo-cv-bridge
GTSAM: Follow installation instructions from here. RTAB-Map needs latest version from source (git clone https://bitbucket.org/gtborg/gtsam.git), it will not build with 3.2.1.

cvsba: Follow installation instructions from here. Their installation is not standard CMake, you need these extra steps so RTAB-Map can find it:

$ mkdir /usr/local/lib/cmake/cvsba 
$ mv /usr/local/lib/cmake/Findcvsba.cmake /usr/local/lib/cmake/cvsba/cvsbaConfig.cmake
Freenect2: Follow installation instructions from here.

Install the RTAB-Map standalone libraries (don't checkout in the Catkin workspace but install in your Catkin's devel folder).

$ cd ~
$ git clone https://github.com/introlab/rtabmap.git rtabmap
$ cd rtabmap/build
$ cmake -DCMAKE_INSTALL_PREFIX=~/catkin_ws/devel ..  [<---double dots included]
$ make -j4
$ make install
Install the RTAB-Map ros-pkg in your src folder of your Catkin workspace.

$ cd ~/catkin_ws
$ git clone https://github.com/introlab/rtabmap_ros.git src/rtabmap_ros
$ catkin_make
